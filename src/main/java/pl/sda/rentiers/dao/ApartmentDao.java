package pl.sda.rentiers.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.rentiers.logic.HibernateUtil;
import pl.sda.rentiers.models.Apartment;
import pl.sda.rentiers.models.IBaseEntity;
import pl.sda.rentiers.models.Tenant;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ApartmentDao {
    public List<Apartment> getListOfApartments(boolean getAll) {
        List<Apartment> list = new ArrayList<>();

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Apartment> query = builder.createQuery(Apartment.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Apartment> tableRoot = query.from(Apartment.class);

            // wykonuję zapytanie(query) w tabeli (tableRoot)
            if (getAll) {
                query.select(tableRoot);
            } else {
                query.select(tableRoot).where(builder.not(builder.equal(tableRoot.get("available"), false)));
            }

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return list;
    }
}
