package pl.sda.rentiers.servlets;

import pl.sda.rentiers.dao.ApartmentDao;
import pl.sda.rentiers.logic.EntityDao;
import pl.sda.rentiers.models.Apartment;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.parser.Entity;
import java.io.IOException;

@WebServlet(urlPatterns = "/apartments_list")
public class ApartmentList extends HttpServlet {
    private EntityDao dao;
    private ApartmentDao apartmentDao;

    @Override
    public void init() throws ServletException {
        dao = new EntityDao();
        apartmentDao = new ApartmentDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String getUnavailableParameter = req.getParameter("unavailableCheckbox");
        boolean loadAll = getUnavailableParameter != null && !getUnavailableParameter.isEmpty() && getUnavailableParameter.equalsIgnoreCase("on");

        req.setAttribute("apartmentsList", apartmentDao.getListOfApartments(loadAll));
        req.setAttribute("getUnavailableStatus", loadAll);

        req.getRequestDispatcher("/listApartments.jsp").forward(req, resp);
    }
}
