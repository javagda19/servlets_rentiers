package pl.sda.rentiers.servlets;

import pl.sda.rentiers.dao.ApartmentDao;
import pl.sda.rentiers.logic.EntityDao;
import pl.sda.rentiers.models.Apartment;
import pl.sda.rentiers.models.Tenant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

@WebServlet(urlPatterns = "/tenants_add")
public class TenantAdd extends HttpServlet {
    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private ApartmentDao apartmentDao;
    private EntityDao entityDao;

    @Override
    public void init() throws ServletException {
        apartmentDao = new ApartmentDao();
        entityDao = new EntityDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String apartmentId = req.getParameter("apartmentId");
        if (apartmentId == null) {
            redirectBack(req, resp);
            return;
        }

        Optional<Apartment> apartment = entityDao.getById(Apartment.class, Long.parseLong(apartmentId));
        if (!apartment.isPresent()) {
            redirectBack(req, resp);
            return;
        }

        req.setAttribute("apartment", apartment.get());
        req.getRequestDispatcher("/addTenants.jsp").forward(req, resp);
    }

    private void redirectBack(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(req.getHeader("referer"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String apartmentId = req.getParameter("apartmentId");
        if (apartmentId == null) {
            resp.sendRedirect("tenants_add");
            return;
        }
        Optional<Apartment> apartmentOptional = entityDao.getById(Apartment.class, Long.parseLong(apartmentId));
        if (!apartmentOptional.isPresent()) {
            resp.sendRedirect("tenants_add");
            return;
        }
        Apartment apartment = apartmentOptional.get();

        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String documentId = req.getParameter("documentId");
        String agreementStartDate = req.getParameter("agreementStartDate");
        String agreementEndDate = req.getParameter("agreementEndDate");
        String rent = req.getParameter("rent");
        String deposited = req.getParameter("deposited");
        String paymentDayOfMonth = req.getParameter("paymentDayOfMonth");

        if (!name.isEmpty() && !surname.isEmpty() &&
                !documentId.isEmpty() && !agreementStartDate.isEmpty() &&
                !agreementEndDate.isEmpty() && !rent.isEmpty() &&
                !deposited.isEmpty() && !paymentDayOfMonth.isEmpty()) {

            Tenant tenant = new Tenant(name,
                    surname,
                    documentId,
                    LocalDate.parse(agreementStartDate, DATE_TIME_FORMATTER),
                    LocalDate.parse(agreementEndDate, DATE_TIME_FORMATTER),
                    Double.parseDouble(rent),
                    Double.parseDouble(deposited),
                    Integer.parseInt(paymentDayOfMonth));

            tenant.setApartments(new HashSet<>(Arrays.asList(apartment)));
            apartment.getTenants().add(tenant);

            entityDao.save(tenant);
            entityDao.save(apartment);
        }
        resp.sendRedirect("apartments_list");
    }
}
