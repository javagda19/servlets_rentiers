package pl.sda.rentiers.servlets;

import pl.sda.rentiers.dao.ApartmentDao;
import pl.sda.rentiers.logic.EntityDao;
import pl.sda.rentiers.models.Apartment;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/apartment_details")
public class ApartmentDetails extends HttpServlet {
    private ApartmentDao apartmentDao;
    private EntityDao entityDao;

    @Override
    public void init() throws ServletException {
        apartmentDao = new ApartmentDao();
        entityDao = new EntityDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String apartmentId = req.getParameter("apartmentId");
        if (apartmentId == null) {
            redirectBack(req, resp);
            return;
        }

        Optional<Apartment> apartment = entityDao.getById(Apartment.class, Long.parseLong(apartmentId));
        if (!apartment.isPresent()) {
            redirectBack(req, resp);
            return;
        }

        req.setAttribute("apartment", apartment.get());
        req.getRequestDispatcher("/detailsApartments.jsp").forward(req, resp);
    }

    private void redirectBack(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(req.getHeader("referer"));
    }
}
