package pl.sda.rentiers.servlets;

import pl.sda.rentiers.logic.EntityDao;
import pl.sda.rentiers.models.Apartment;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(urlPatterns = "/apartments_add")
public class ApartmentAdd extends HttpServlet {
    private EntityDao entityDao;

    @Override
    public void init() throws ServletException {
        entityDao = new EntityDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/addApartments.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String address = req.getParameter("address");
        String zipCode = req.getParameter("zipCode");
        String city = req.getParameter("city");
        String size = req.getParameter("size");
        String roomsNumber = req.getParameter("roomsNumber");
        String lastRenovated = req.getParameter("lastRenovated");
        String value = req.getParameter("value");
        String floor = req.getParameter("floor");
        String nonSmoking = req.getParameter("nonSmoking");
        String noPets = req.getParameter("noPets");
        String available = req.getParameter("available");

        if (!address.isEmpty() && !zipCode.isEmpty() &&
                !city.isEmpty() && !size.isEmpty() &&
                !roomsNumber.isEmpty() && !lastRenovated.isEmpty() &&
                !value.isEmpty() && !floor.isEmpty()) {

            Apartment apartment = new Apartment(address,
                    zipCode,
                    city,
                    Double.parseDouble(size),
                    Integer.parseInt(roomsNumber),
                    LocalDate.parse(lastRenovated),
                    Double.parseDouble(value),
                    Integer.parseInt(floor),
                    nonSmoking != null && nonSmoking.equalsIgnoreCase("on"),
                    noPets != null && noPets.equalsIgnoreCase("on"),
                    available != null && available.equalsIgnoreCase("on"));

            entityDao.save(apartment);
        }
        resp.sendRedirect("apartments_list");
    }
}
