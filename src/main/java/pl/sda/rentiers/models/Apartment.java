package pl.sda.rentiers.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public
class Apartment implements IBaseEntity {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinTable(name = "Apartment_Tenant",
//            joinColumns = @JoinColumn(name = "apartments_id"),
//            inverseJoinColumns = @JoinColumn(name = "tenants_id"))
//    @Where(clause = "agreementStartDate <= current_date() <= agreementEndDate")
//    private Tenant currentTenant;

    private String address;
    private String zipcode;
    private String city;
    private Double size;
    private Integer rooms;
    private LocalDate lastRenovation;
    private Double value;
    private Integer floor;
    private Boolean nonSmoking;
    private Boolean noPets;

    @Column(name = "available")
    private Boolean available;

    @OneToMany(mappedBy = "apartment")
    private Set<Unavailable> unavailables;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Tenant> tenants;

    @OneToMany(mappedBy = "apartment")
    private Set<Payment> payments;

    public Apartment(String address, String zipcode, String city, double size, int rooms, LocalDate lastRenovation, double value, int floor, boolean nonSmoking, boolean noPets, boolean available) {
        this.address = address;
        this.zipcode = zipcode;
        this.city = city;
        this.size = size;
        this.rooms = rooms;
        this.lastRenovation = lastRenovation;
        this.value = value;
        this.floor = floor;
        this.nonSmoking = nonSmoking;
        this.noPets = noPets;
        this.available = available;
    }

    public Optional<Tenant> getCurrentTenant() {
        return tenants.stream().filter(tenant ->
                tenant.getAgreementStartDate().isBefore(LocalDate.now()) &&
                        tenant.getAgreementEndDate().isAfter(LocalDate.now())).findFirst();
    }
}