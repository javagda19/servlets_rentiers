package pl.sda.rentiers.models;

public enum PaymentTypeName {
    CARD, CHECK, OTHER, TRANSFER
}
