package pl.sda.rentiers.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Generated;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Payment implements IBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double amount;
    private Integer daysLate;
    private String paymentMethod;
    private String note;
    private LocalDate paymentDate;

    @ManyToMany(mappedBy = "payments")
    private Set<PaymentType> paymentTypes;

    @ManyToOne
    private Apartment apartment;

    @ManyToOne
    private Tenant tenant;

    public Payment(double amount, int daysLate, String paymentMethod, String note, LocalDate paymentDate) {
        this.amount = amount;
        this.daysLate = daysLate;
        this.paymentMethod = paymentMethod;
        this.note = note;
        this.paymentDate = paymentDate;
    }
}