package pl.sda.rentiers.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Tenant implements IBaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String surname;
    private String documentId;
    private LocalDate agreementStartDate;
    private LocalDate agreementEndDate;
    private Double rent;
    private Double deposited;
    private int paymentDayOfMonth;

    @ManyToMany (mappedBy = "tenants")
    private Set<Apartment> apartments;

    @OneToMany (mappedBy = "tenant")
    private Set<Payment> payments;

    public Tenant(String name, String surname, String documentId, LocalDate agreementStartDate, LocalDate agreementEndDate, double rent, double deposited, int paymentDayOfMonth) {
        this.name = name;
        this.surname = surname;
        this.documentId = documentId;
        this.agreementStartDate = agreementStartDate;
        this.agreementEndDate = agreementEndDate;
        this.rent = rent;
        this.deposited = deposited;
        this.paymentDayOfMonth = paymentDayOfMonth;
    }
}