package pl.sda.rentiers.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class PaymentType implements IBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    private PaymentTypeName typeName;

    @ManyToMany
    private Set<Payment> payments;

    public PaymentType(PaymentTypeName typeName) {
        this.typeName = typeName;
    }
}
