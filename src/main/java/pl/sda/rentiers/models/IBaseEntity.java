package pl.sda.rentiers.models;

public interface IBaseEntity {
    public Long getId();
}
