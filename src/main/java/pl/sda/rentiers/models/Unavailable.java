package pl.sda.rentiers.models;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Unavailable implements IBaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate startDate;
    private LocalDate stopDate;
    private String reason;

    @ManyToOne
    private Apartment apartment;


    public Unavailable(LocalDate startDate, LocalDate stopDate, String reason) {
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.reason = reason;
    }
}