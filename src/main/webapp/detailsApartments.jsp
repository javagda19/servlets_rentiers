<%@ page import="pl.sda.rentiers.models.Apartment" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pl.sda.rentiers.models.Tenant" %>
<%@ page import="java.util.Optional" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/19/19
  Time: 11:01 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Apartments</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>
<%
    Object apartmentAttribute = request.getAttribute("apartment");
    Apartment apartment = (Apartment) apartmentAttribute;
    Tenant currentTenant = apartment.getCurrentTenant().orElse(null);
%>

<div class="container">
    <div class="row marginless additional-space">

        <div class="col-md-6 bordered additional-space"><%--prawa--%>
            <fieldset>
                <div class="row col-md-12 marginless">
                    <legend>Apartment details:</legend>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label class="label-alignment-middle" for="address">Address</label>
                    </div>
                    <div class="col-md-6">
                        <label id="address" name="address" class="form-control">
                            <%=apartment.getAddress()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="zipCode">Postal Code</label></div>
                    <div class="col-md-6">
                        <label id="zipCode" name="zipCode" class="form-control">
                            <%=apartment.getZipcode()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="city">City</label></div>
                    <div class="col-md-6">
                        <label id="city" name="city" class="form-control">
                            <%=apartment.getCity()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="size">Size</label>
                    </div>
                    <div class="col-md-6">
                        <label id="size" name="size" class="form-control">
                            <%=apartment.getSize()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="roomsNumber">Number Of Rooms</label>
                    </div>
                    <div class="col-md-6">
                        <label id="roomsNumber" name="roomsNumber" class="form-control">
                            <%=apartment.getRooms()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="lastRenovated">Last Renovated</label></div>
                    <div class="col-md-6">
                        <label id="lastRenovated" name="lastRenovated" class="form-control">
                            <%=apartment.getLastRenovation()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="value">Value</label>
                    </div>
                    <div class="col-md-6">
                        <label id="value" name="value" class="form-control">
                            <%=apartment.getValue()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="floor">Floor</label>
                    </div>
                    <div class="col-md-6">
                        <label id="floor" name="floor" class="form-control">
                            <%=apartment.getFloor()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="nonSmoking">Non-Smoking</label>
                    </div>
                    <div class="col-md-6">
                        <label id="nonSmoking" name="nonSmoking" class="form-control">
                            <%=apartment.getNonSmoking()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="noPets">No Pets</label>
                    </div>
                    <div class="col-md-6">
                        <label id="noPets" name="noPets" class="form-control">
                            <%=apartment.getNoPets()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="owned">Owned</label></div>
                    <div class="col-md-6">
                        <label id="owned" name="owned" class="form-control">
                            <%=apartment.getAvailable()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless" id="div-line-margin">
                </div>

                <% if (currentTenant != null) {%>
                <div class="row col-md-12 marginless">
                    <legend>Current tenant details:</legend>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label class="label-alignment-middle" for="name">Name</label>
                    </div>
                    <div class="col-md-6">
                        <label id="name" name="name" class="form-control">
                            <%=currentTenant.getName()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="surname">
                            Surname
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label id="surname" name="surname" class="form-control">
                            <%=currentTenant.getSurname()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="documentId">Document Id</label></div>
                    <div class="col-md-6">
                        <label id="documentId" name="documentId" class="form-control">
                            <%=currentTenant.getDocumentId()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="agreementStartDate">Agreement start date</label>
                    </div>
                    <div class="col-md-6">
                        <label id="agreementStartDate" name="agreementStartDate" class="form-control">
                            <%=currentTenant.getAgreementStartDate()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="agreementEndDate">
                            Agreement end date
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label id="agreementEndDate" name="agreementEndDate" class="form-control">
                            <%=currentTenant.getAgreementEndDate()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="rent">
                            Rent
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label id="rent" name="rent" class="form-control">
                            <%=currentTenant.getRent()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="deposited">Deposited</label>
                    </div>
                    <div class="col-md-6">
                        <label id="deposited" name="deposited" class="form-control">
                            <%=currentTenant.getDeposited()%>
                        </label>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="paymentDayOfMonth">Payment day of the month</label>
                    </div>
                    <div class="col-md-6">
                        <label id="paymentDayOfMonth" name="paymentDayOfMonth" class="form-control">
                            <%=currentTenant.getPaymentDayOfMonth()%>
                        </label>
                    </div>
                </div>
                <%}%>
            </fieldset>
        </div>


        <div class="offset-md-1 col-md-5 bordered additional-space"><%--lewa --%>
            <div class="row marginless col-12">
                <h2>Tenants:</h2>
            </div>
            <div class="row marginless col-12 header-row">
                <div class="col-1">Lp.</div>
                <div class="col-1">Id.</div>
                <div class="col-6">Address</div>
                <div class="col-4">Actions</div>
            </div>
            <%
                int i = 0;
                for (Tenant tenant : apartment.getTenants()) {
            %>
            <div class="row col-12 marginless table-row">
                <div class="col-1">
                    <%= ++i %>
                </div>
                <div class="col-1">
                    <%=tenant.getId()%>
                </div>
                <div class="col-6">
                    <%=tenant.getName() + ", " + tenant.getSurname()%>
                </div>
                <div class="col-4">
                    <form action="tenant_details" method="get">
                        <input type="text" value="<%=tenant.getId()%>" name="tenantId" hidden>
                        <input class="col-12 form-control" type="submit" value="Details">
                    </form>
                </div>
            </div>
            <%}%>

            <div class="row col-12 marginless" id="button-margin">
            </div>

            <div class="row marginless col-12">
                <form action="tenants_add" method="get" class="col-12 paddingless">
                    <input hidden type="text" value="<%=apartment.getId()%>" name="apartmentId">
                    <input class="form-control col-12" type="submit" value="Add new tenant" id="test-radzio">
                </form>
            </div>
        </div>
    </div>
</div>

<%--<script type="application/javascript">--%>
<%--document.getElementById('test-radzio').addEventListener('onclick', function (ev) {--%>
<%--document.getElementById('test-radzio').disabled = true;--%>
<%--});--%>
<%--</script>--%>

<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
