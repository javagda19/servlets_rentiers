<%@ page import="pl.sda.rentiers.models.Apartment" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/19/19
  Time: 11:01 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Apartments</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>
<%
    Object apartmentAttribute = request.getAttribute("apartmentsList");
    Boolean unavailableLoaded = Boolean.parseBoolean(request.getAttribute("getUnavailableStatus").toString());
    List<Apartment> apartmentList;
    if (apartmentAttribute == null) {
        apartmentList = new ArrayList<>();
    } else {
        apartmentList = (List<Apartment>) apartmentAttribute;
    }
%>

<div class="container">
    <div class="row marginless">
        <div class="col-md-6 bordered "><%--lewa --%>
            <div class="row marginless col-12">
                <h2>Apartments:</h2>
            </div>
            <div class="row marginless col-12 header-row">
                <div class="col-1">Lp.</div>
                <div class="col-1">Id.</div>
                <div class="col-6">Address</div>
                <div class="col-4">Actions</div>
            </div>
            <% for (int i = 0; i < apartmentList.size(); i++) {%>
            <% Apartment apartment = apartmentList.get(i); %>
            <div class="row col-12 marginless table-row">
                <div class="col-1">
                    <%=i + 1%>
                </div>
                <div class="col-1">
                    <%=apartment.getId()%>
                </div>
                <div class="col-6">
                    <%=apartment.getAddress() + ", " + apartment.getZipcode() + " " + apartment.getCity()%>
                </div>
                <div class="col-4">
                    <form action="apartment_details" method="get">
                        <input type="text" value="<%=apartment.getId()%>" name="apartmentId" hidden>
                        <input class="col-12 form-control" type="submit" value="Details">
                    </form>
                </div>
            </div>
            <%}%>
        </div>
        <div class="offset-md-1 col-md-5 bordered"><%--prawa--%>
            <form method="get" class="col-12 paddingless">
                <div class="row marginless col-12">
                    <div class="col-8 const-height">
                        <label class="label-alignment-middle" for="unavailableCheckbox">Print unavailable
                            apartments:</label>
                    </div>
                    <div class="col-4 const-height">
                        <input class="form-control label-alignment-middle" name="unavailableCheckbox"
                               id="unavailableCheckbox" type="checkbox" <%=unavailableLoaded?"checked":""%>>
                    </div>
                </div>
                <div class="row marginless col-12">
                    <input class="form-control" type="submit" value="Reload list">
                </div>
            </form>
            <div class="row col-12" id="button-margin">
            </div>

            <div class="row marginless col-12">
                <form action="apartments_add" method="get" class="col-12 paddingless">
                    <input class="form-control col-12" type="submit" value="Add new apartment" id="test-radzio">
                </form>
            </div>
        </div>
    </div>
</div>

<%--<script type="application/javascript">--%>
    <%--document.getElementById('test-radzio').addEventListener('onclick', function (ev) {--%>
        <%--document.getElementById('test-radzio').disabled = true;--%>
    <%--});--%>
<%--</script>--%>

<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
