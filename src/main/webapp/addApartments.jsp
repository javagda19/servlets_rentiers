<%--
  Created by IntelliJ IDEA.
  User: czarn
  Date: 18/05/2019
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Apartment Form</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>
<div class="input-wrapper container" <%--style="width:33%; margin:auto"--%>>
    <form class="pure-form pure-form-stacked" action="apartments_add" method="post">
        <fieldset>
            <legend>Add apartment to database.</legend>
            <div class="row col-12">
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="address">
                            Address
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="address" name="address" class="form-control" type="text" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="zipCode">
                            Postal Code
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="zipCode" name="zipCode" class="form-control" type="text" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="city">
                            City
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="city" name="city" class="form-control" type="text" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="size">
                            Size
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="size" name="size" class="form-control" type="text" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="roomsNumber">
                            Number Of Rooms
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="roomsNumber" name="roomsNumber" class="form-control" type="text" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="lastRenovated">
                            Last Renovated
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="lastRenovated" name="lastRenovated" class="form-control" type="date" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="value">
                            Value
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="value" name="value" class="form-control" type="number" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="floor">
                            Floor
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="floor" name="floor" class="form-control" type="number" required>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="nonSmoking">
                            Non-Smoking
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="nonSmoking" name="nonSmoking" class="form-control" type="checkbox" checked>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="noPets">
                            No Pets
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="noPets" name="noPets" class="form-control" type="checkbox" checked>
                    </div>
                </div>
                <div class="row col-md-12 marginless">
                    <div class="col-md-6">
                        <label for="available">
                            Available
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input id="available" name="available" class="form-control" type="checkbox" checked>
                    </div>
                </div>
                <input type="submit" class="form-control" value="Submit ">
        </fieldset>
    </form>
</div>
<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
