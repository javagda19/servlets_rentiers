<%@ page import="pl.sda.rentiers.models.Apartment" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 18/05/2019
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Tenant Form</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>
<%
    Object apartmentAttribute = request.getAttribute("apartment");
    Apartment apartment = (Apartment) apartmentAttribute;
%>

<div class="container">
    <div class="row marginless">
        <form action="tenants_add" method="POST">
            <fieldset>
                <legend>Add tenant to apartment.</legend>
                <div class="row marginless">
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="desc">
                                Adding tenant to apartment:
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="desc" name="desc" class="form-control" type="text"
                                   value="<%=apartment.getAddress() + ", " + apartment.getZipcode() +", " + apartment.getCity()%>"
                                   disabled>
                            <input id="apartmentId" name="apartmentId" class="form-control"
                                   value="<%=apartment.getId()%>"
                                   type="hidden" hidden>
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="name">
                                Name
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="name" name="name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="surname">
                                Surname
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="surname" name="surname" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="documentId">
                                Document Id
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="documentId" name="documentId" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="agreementStartDate">
                                Agreement start date
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="agreementStartDate" name="agreementStartDate" class="form-control" type="date"
                            >
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="agreementEndDate">
                                Agreement end date
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="agreementEndDate" name="agreementEndDate" class="form-control" type="date">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="rent">
                                Rent
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="rent" name="rent" class="form-control" type="number">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="deposited">
                                Deposited
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="deposited" name="deposited" class="form-control" type="number">
                        </div>
                    </div>
                    <div class="row col-md-12 marginless">
                        <div class="col-md-6">
                            <label for="paymentDayOfMonth">
                                Payment day of the month
                            </label>
                        </div>
                        <div class="col-md-6">
                            <input id="paymentDayOfMonth" name="paymentDayOfMonth" class="form-control" type="number"
                            >
                        </div>
                    </div>
                    <input type="submit" class="form-control" value="Submit Tenant">
                </div>
            </fieldset>
        </form>
    </div>
</div>
<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
